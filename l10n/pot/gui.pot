#TranslationFunction AudeGUI

msgid  "Program Result"
msgstr ""

msgid  "\nStack trace: \n"
msgstr ""

msgid  "Error on line {0}, character {1}"
msgstr ""

msgid  "Error on line {0}"
msgstr ""

msgid  "Execute the current automaton with a word"
msgstr ""

msgid  ""
"Which name do you want to give to the exported file? (give a .dot extension "
"to save as dot format, .svg to save as svg, .txt to save as automaton code)"
msgstr ""

msgid  "Which name do you want to give to the exported text file?"
msgstr ""

msgid  "get_automaton: automaton n{0} could not be understood."
msgstr ""

msgid  ""
"get_automaton: automaton n{0} doesnt exist or doesnt have an initial state."
msgstr ""

msgid  "Please enter a name for the file in which the program will be saved."
msgstr ""

msgid  ""
"Please enter a name for the file in which the automaton will be saved (give "
"a .dot extension to save as dot format, .svg to save as svg, .txt to save in "
"Aude format)."
msgstr ""

msgid  "Syntax error: bad import parameter in {0}"
msgstr ""

msgid  "Error: import of {0} in {2} failed: '{1}'."
msgstr ""

msgid  ""
"Error: import: absolute paths are not supported in this version (in '{0}')'"
msgstr ""

msgid  "Mode:"
msgstr ""

msgid  "Execute Program"
msgstr ""

msgid  "Design"
msgstr ""

msgid  "Program"
msgstr ""

msgid  "Automaton code"
msgstr ""

msgid  "Open"
msgstr ""

msgid  "Save"
msgstr ""

msgid  "Save As"
msgstr ""

msgid  "Automaton:"
msgstr ""

msgid  "Program:"
msgstr ""

msgid  "Export"
msgstr ""

msgid  "Redraw"
msgstr ""

msgid  "Run a word"
msgstr ""

msgid  "Export result"
msgstr ""

msgid  "Automaton n"
msgstr ""

msgid  "result.txt"
msgstr ""

msgid  "automaton.txt"
msgstr ""

msgid  "Word: "
msgstr ""

msgid  "Run"
msgstr ""

msgid  "Step"
msgstr ""

msgid  "Delay between steps(ms): "
msgstr ""

msgid  "Edit this automaton"
msgstr ""

msgid  ""
"To add a new state, double-click on the drawing area where you want it to be."
msgstr ""

msgid  ""
"To add a new transition, Shift+click on the start state then click on the "
"destination state.<br /><a href='#' onclick='return helpSymbols()'>Show how "
"to input symbols</a>."
msgstr ""

msgid  "To rename a state, double-click on it."
msgstr ""

msgid  ""
"To modify symbols of a transition, double-click on its label.<br /><a "
"href='#' onclick='return helpSymbols()'>Show how to input symbols</a>."
msgstr ""

msgid  "To remove a state, Ctrl+click on it."
msgstr ""

msgid  "To remove a transition, Ctrl+click on its arrow."
msgstr ""

msgid  "To set a state as (non-)accepting, right-click on it."
msgstr ""

msgid  "To set a state as the initial state, Ctrl+right-click on it."
msgstr ""

msgid  "To make a transition straight, Shift+click on its arrow or its label."
msgstr ""

msgid  "New state"
msgstr ""

msgid  "New transition"
msgstr ""

msgid  "Modify state's name"
msgstr ""

msgid  "Modify transitions' symbols"
msgstr ""

msgid  "Remove a state"
msgstr ""

msgid  "Remove a transition"
msgstr ""

msgid  "Toggle accepting/non accepting"
msgstr ""

msgid  "Set a state as initial"
msgstr ""

msgid  "Make a transition straight"
msgstr ""

msgid  "Toggle help"
msgstr ""

msgid  "Howto: input symbols"
msgstr ""

msgid  ""
"<p>In the window which will invite you to input symbols, simply enter the "
"symbol you want to attach to the transition.</p><p>If you want to attach "
"more than one symbol, separate them with commas.</p><p>If you want to input "
"symbols containing spaces or commas, surround them with double quotes.</"
"p><p>If you need to input a symbol containing double-quotes or slashes, put "
"a slash behind them and surround the symbol with double-quotes.</p><p>to "
"insert an epsilon (-transition), you can input it directly or use <code>\\e</"
"code></p>"
msgstr ""

msgid  "Create a new automaton"
msgstr ""

msgid  "Remove current automaton"
msgstr ""

msgid  "There is no automaton to save."
msgstr ""

msgid  "Automaton"
msgstr ""

msgid  "Determinize"
msgstr ""

msgid  "Simplify names of the states"
msgstr ""

msgid  "-eliminate"
msgstr ""

msgid  "Minimize"
msgstr ""

msgid  "Complicate"
msgstr ""

msgid  "Mutate"
msgstr ""

msgid  "List differentiable states"
msgstr ""

msgid  "List the reachable states"
msgstr ""

msgid  "List the co-reachable states"
msgstr ""

msgid  "Test empty language"
msgstr ""

msgid  "Test infinite language"
msgstr ""

msgid  "Moore machine  Mealy machine"
msgstr ""

msgid  "Mealy machine  Moore machine"
msgstr ""

msgid  ""
"Complicate the current automaton by choosing one or more of the following "
"options\n"
"1 - Add a non reachable state\n"
"2 - Add a transition between two non reachable states\n"
"3 - Add a transition to a not co-reachable state\n"
"4 - Copy transitions"
msgstr ""

msgid  ""
"Transform the current automaton by choosing one or more of the following "
"options:\n"
"1 - Change a symbol\n"
"2 - Remove a transition\n"
"3 - Add a transition\n"
"4 - Add a state and some transitions\n"
"5 - Inverse a transition\n"
"6 - Rename states\n"
"7 - Make a state non accepting\n"
"8 - Remove an epsilon transition\n"
"9 - Remove a state\n"
"10 - Make a state accepting"
msgstr ""

msgid  "JSON representation"
msgstr ""

msgid  "Table representation"
msgstr ""

msgid  "Load a Quiz"
msgstr ""

msgid  "Loading the quiz failed"
msgstr ""

msgid  "The quiz seems to be malformed: {0}"
msgstr ""

msgid  "Start the Quiz"
msgstr ""

msgid  "Close the Quiz"
msgstr ""

msgid  "Close"
msgstr ""

msgid  "Quiz:"
msgstr ""

msgid  "Quiz"
msgstr ""

msgid  "Question {0}: "
msgstr ""

msgid  "Next question"
msgstr ""

msgid  "Previous question"
msgstr ""

msgid  "The Quiz is finished! Here are the details of the correction."
msgstr ""

msgid  "Error in the Quiz"
msgstr ""

msgid  "There is an error in the Quiz: {0}"
msgstr ""

msgid  "Question {0} has no answers."
msgstr ""

msgid  "You can draw the automaton bellow."
msgstr ""

msgid  "Equiv. between 2 automata"
msgstr ""

msgid  "Reg. Exp.  automaton"
msgstr ""

msgid  "Reg. Exp.  minimized automaton"
msgstr ""

msgid  "Automaton  Reg. Exp."
msgstr ""

msgid  "Complete"
msgstr ""

msgid  "Complement"
msgstr ""

msgid  "Product"
msgstr ""

msgid  "Mirror"
msgstr ""

msgid  "Wrong answer for {0}."
msgstr ""

msgid  "The given automaton accepts too many words."
msgstr ""

msgid  "The empty word is accepted while it shouldnt be."
msgstr ""

msgid  "The empty word is not accepted while it should be."
msgstr ""

msgid  "Word <i>{0}</i> is accepted while it shouldnt be."
msgstr ""

msgid  "Word <i>{0}</i> is not accepted while it should be."
msgstr ""

msgid  "The given automaton isn’t equivalent to the expected one."
msgstr ""

msgid  "The given regular expression isn’t equivalent to the expected one."
msgstr ""

msgid  "The given automaton isn’t determinized."
msgstr ""

msgid  "The given automaton isn’t minimized."
msgstr ""

msgid  "The given automaton isn’t complete."
msgstr ""

msgid  "Question type not known"
msgstr ""

msgid  ""
"Type of question {0} is not known. Known types are: <ul><li>\"mcq\" for "
"multiple choices question,</li><li>\"word\" (to draw an automaton which "
"accepts a given list of words).</li></ul>"
msgstr ""

msgid  "Question was not answered."
msgstr ""

msgid  ""
"We are willing to dont give you any mark. Your progress is the most "
"important thing, above any arbitrary absolute meaningless mark. Keep your "
"efforts;-)"
msgstr ""

msgid  "Correct answer?"
msgstr ""

msgid  "Comments"
msgstr ""

msgid  "Instruction"
msgstr ""

msgid  "Yes"
msgstr ""

msgid  "No"
msgstr ""

msgid  "Previous page"
msgstr ""

msgid  "Automaton given in the quiz is not correct."
msgstr ""

msgid  "Regular expression given in the quiz is not valid."
msgstr ""

msgid  "No automaton or regular expression was given in the quiz."
msgstr ""

msgid  "No regular expression or automaton was given in the quiz."
msgstr ""

msgid  "Select an algo"
msgstr ""

msgid  "You didnt select enough automata. Please select {0} automata."
msgstr ""

msgid  "Automaton #{0}"
msgstr ""

msgid  "You can choose the order in which automata will be used in algorithms."
msgstr ""

msgid  "Continue execution"
msgstr ""

msgid  ""
"The algorithm you want to use needs {0} automata. Please select these "
"automata in the order you want and click \"Continue execution\" when you are "
"ready."
msgstr ""

msgid  ""
"This order will be used for future algorithm executions. If you want to "
"change this order, you can call this list using the <img src=\"{0}\" /> "
"toolbar icon.<br />Notice: Algorithms taking only one automaton work with "
"the current automaton, they dont use this ordering."
msgstr ""

msgid  ""
"The file was not found or you don't have enough permissions to read it. "
"(HTTP status: {0})"
msgstr ""

msgid  "Unable to get the list of predefined algorithms"
msgstr ""

msgid  "Ready to use quizzes"
msgstr ""

msgid  "Built-in algorithms"
msgstr ""

msgid  "Examples of automaton"
msgstr ""

msgid  "Load a quiz"
msgstr ""

msgid  "Load a program"
msgstr ""

msgid  "Load an automaton"
msgstr ""

msgid  "Loading program failed."
msgstr ""

msgid  "Loading automaton failed."
msgstr ""

msgid  "Loading quiz failed."
msgstr ""

msgid  "From your computer"
msgstr ""

msgid  "Choose and order the parameter automata"
msgstr ""

msgid  "Order the parameter automata"
msgstr ""

msgid  ""
"<h2>Welcome to {0}!</h2><h3> Never used {0} before? </h3><ul>    <li>Create "
"your first automaton by clicking on the <strong>New state</strong> button at "
"the bottom left of the screen.</li>    <li>You can apply an algorithm on "
"your automaton with the <strong>Select an algo</strong> toolbar button.</"
"li></ul><h3> Using a keyboard and a mouse? You can be faster. </h3><ul>    "
"<li>To add a <strong>new state</strong>, double-click where you want the "
"state to be.</li>    <li>To add a <strong>new transition</strong>, Shift"
"+click on the start state then click on the destination state.</li>    "
"<li>To <strong>rename</strong> a state, to <strong>modify symbols</strong> "
"of a transition, double-click on it.</li>    <li>To set a state as the "
"<strong>initial</strong> state, ctrl+right click on the state.</li>    "
"<li>To set a state as <strong>(non-)accepting</strong>, right-click on it.</"
"li>    <li>To <strong>remove</strong> a state or a transition, ctrl-click on "
"it.</li></ul>"
msgstr ""

msgid  ""
"<h3> Quizzes </h3><p>To load a quiz, click on the \"Load a Quiz\" toolbar "
"button. You can keep on using all the features of the program, like running "
"algorithms, during the quiz whenever it is possible to draw an automaton.</"
"p><p> Now its your turn!</p>"
msgstr ""

msgid  "Unable to access the code of automaton n{0}"
msgstr ""

msgid  "You need to fix the automaton in design mode before accessing its code."
msgstr ""

msgid  "Zoom and center automatically"
msgstr ""

msgid  "Undo"
msgstr ""

msgid  "Redo"
msgstr ""

msgid  "Menu"
msgstr ""

msgid  "Add a question"
msgstr ""

msgid  "Edit or Create  a Quiz"
msgstr ""

msgid  "Example: Which of the following assertions are correct? (check the correct answers)"
msgstr ""

msgid  "Example: For the following languages, give an automaton that recognizes it (if such an automaton exists)."
msgstr ""

msgid  "Example: For the following minimal automaton, give a regular expression that is equivalent."
msgstr ""

msgid  "Multiple Choice Question"
msgstr ""

msgid  "Find an automaton"
msgstr ""

msgid  "Find a regular expression"
msgstr ""

msgid  "Search…"
msgstr ""

msgid  "Close the Quiz Editor"
msgstr ""

msgid  "Questions"
msgstr ""

msgid  "Question"
msgstr ""

msgid  "Answers"
msgstr ""

msgid  "Answer"
msgstr ""

msgid  "Points"
msgstr ""

msgid  "Edit"
msgstr ""

msgid  "Nb"
msgstr ""

msgid  "Quiz Editor"
msgstr ""

msgid  "New quiz"
msgstr ""

msgid  "Do you really want to start a new quiz from scratch?"
msgstr ""

msgid  "Do you really want to abandon your modifications to the quiz?"
msgstr ""

msgid  "Write the different possible answers and check the correct ones."
msgstr ""

msgid  "Choice {0}…"
msgstr ""

msgid  "New Question"
msgstr ""

msgid  "Add"
msgstr ""

msgid  "Add an answer"
msgstr ""

msgid  "Validate"
msgstr ""

msgid  "Show the quiz overview"
msgstr ""

msgid  "Quiz Overview"
msgstr ""

msgid  "January"
msgstr ""

msgid  "February"
msgstr ""

msgid  "March"
msgstr ""

msgid  "April"
msgstr ""

msgid  "May"
msgstr ""

msgid  "June"
msgstr ""

msgid  "July"
msgstr ""

msgid  "August"
msgstr ""

msgid  "September"
msgstr ""

msgid  "October"
msgstr ""

msgid  "November"
msgstr ""

msgid  "December"
msgstr ""

msgid  "Quiz title:"
msgstr ""

msgid  "Your name:"
msgstr ""

msgid  "Description:"
msgstr ""

msgid  "There is no question in this quiz yet. Click on the button bellow to add a new question."
msgstr ""

msgid  "Write your question…"
msgstr ""

msgid  "Write a regular expression…"
msgstr ""

msgid  "Automaton answer"
msgstr ""

msgid  "Draw an automaton"
msgstr ""

msgid  "Write your audescript code here…"
msgstr ""

msgid  "Write the regular expression…"
msgstr ""

msgid  "Loading…"
msgstr ""

msgid  "Your regular expression is going to be checked."
msgstr ""

msgid  "This regular expresion does not seem valid."
msgstr ""

msgid  "Recognized operations: “.”, “*”, “+”."
msgstr ""

msgid  "If you want the automaton answer to be:"
msgstr ""

msgid  "determinized, write isDeterminized(autoAnsw)"
msgstr ""

msgid  "minimized, write isMinimized(autoAnsw)"
msgstr ""

msgid  "completed, write isCompleted(autoAnsw)"
msgstr ""

msgid  "Initial stack symbol : "
msgstr ""

msgid  "Determinized pushdown automaton: "
msgstr ""

msgid  "State: "
msgstr ""

msgid  "Stacks"
msgstr ""

msgid  "Index"
msgstr ""

msgid  "Value"
msgstr ""

msgid  "Stack"
msgstr ""

msgid  "Write directly the grammar: "
msgstr ""

msgid  "Or enter easily the grammar: "
msgstr ""

msgid  "Write the terminal symbols: "
msgstr ""

msgid  "Write the non terminal symbols: "
msgstr ""

msgid  "Select the start symbol"
msgstr ""

msgid  "Non terminal symbol"
msgstr ""

msgid  "Body"
msgstr ""

msgid  "Choose the entry of the algorithm: "
msgstr ""

msgid  "Choose type"
msgstr ""

msgid  "Enter a grammar"
msgstr ""

msgid  "Automaton determinist: 1\nAutomaton non determinist: 2 \nAutomaton non determinist with ε-transitions: 3"
msgstr ""

msgid  "Settings automaton"
msgstr ""

msgid  "Create the complementary automaton of the following automaton "
msgstr ""

msgid  "Create the complete automaton of the following automaton "
msgstr "Déssiner l'automate complété de l'automate suivant"

msgid  "Do the product of the following automata "
msgstr "Dessiner le produit des 2 automates suivant"

msgid  "Minimize the following automaton "
msgstr "Minimiser l'automate suivant"

msgid  "Write the equivalent states of the following automaton "
msgstr "Ecriver la liste des états équivalents de l'automate suivant"

msgid  "Are the following automatons equivalent? "
msgstr "Est-ce que les automates sont équivalents ?"

msgid  "Fill the table corresponding to the automaton "
msgstr "Remplisser le tableau correspondant à l'automate"

msgid  "Create the automaton corresponding to the following automaton "
msgstr ""

msgid  "Write the reachable states of the following automaton"
msgstr ""

msgid  "Write the co-reachable states of the following automaton"
msgstr ""

msgid  "Write a word recognized by the following automaton"
msgstr "Ecriver un mot reconnu par l'automate"

msgid  "Create the determinized automaton of the following automaton"
msgstr "Dessiner l'automate déterminisé de l'automate suivant"

msgid  "Create the determinized and minimize automaton of the following automaton"
msgstr "Dessiner l'automate déterminisé et minimisé de l'automate suivant"

msgid  "Eleminate the ε-transitions of the following automaton"
msgstr "Eliminer les ε-transitions de l'automate suivant"

msgid  "Eleminate the ε-transitions and determinize the following automaton"
msgstr "Eliminer les ε-transitions et déterminisé de l'automate suivant"

msgid  "Write the regular expression corresponding to the following automaton"
msgstr "Ecriver l'expression régulière correspondant à l'automate"

msgid  "Give the automaton corresponding to the following RE"
msgstr "Donner l'automate correspondant à l'expression régulière"

msgid  "Give the automaton corresponding to the following right linear grammar"
msgstr "Dessiner l'automate correspondant à la grammaire linéaire"

msgid  "Give the linear grammar corresponding to the following automaton"
msgstr "Donner la grammaire linéaire correspondant à l'automate"

msgid  "Give the right linear grammar corresponding to the following left linear grammar"
msgstr "Donner la grammaire linéaire à droite correspondant à la grammaire linéaire à gauche suivante"

msgid  "Settings"
msgstr ""

msgid  "Close the question list"
msgstr "Fermer la page"

msgid  "Chapter 1: Deterministic finite state machines"
msgstr ""

msgid  "Chapter 2: Non-deterministic finite state machines"
msgstr ""

msgid  "Chapter 3: Non-deterministic finite state machines with ε-transitions"
msgstr ""

msgid  "Chapter 4: Regular expressions and Kleene theorem"
msgstr ""

msgid  "Chapter 5: Grammars and regular grammars"
msgstr ""

msgid  "Chapter 6: Non-regular langages and iterative lemma"
msgstr ""

msgid  "No chapter selected."
msgstr ""

msgid  "Question List"
msgstr ""

msgid  "Setting for the questions"
msgstr ""

msgid  "Automaton1"
msgstr ""

msgid  "Automaton2"
msgstr ""

msgid  "Regular expression"
msgstr ""

msgid  "Grammar"
msgstr ""

msgid  "Select file "
msgstr ""

msgid  "Generate randomly "
msgstr ""

msgid  "Get random files "
msgstr ""

msgid  "Automaton generated randomly"
msgstr ""

msgid  "Number of states: "
msgstr ""

msgid  "Alphabet "
msgstr ""

msgid  "Number of accepting states: "
msgstr ""

msgid  "Mode: "
msgstr ""

msgid  "Number of transitions: "
msgstr ""

msgid  "Automaton selection"
msgstr ""

msgid  "Select an automaton from the list or from your computer"
msgstr ""

msgid  "Open automaton"
msgstr ""

msgid  "Selected automaton: "
msgstr ""

msgid  "none"
msgstr ""

msgid  "The automaton is selected randomly from files"
msgstr ""

msgid  "Regular expression generated randomly"
msgstr ""

msgid  "Create a random automaton by using the pararametes of the automaton 1 and converts it to a RE"
msgstr ""

msgid  "Prefearbly select a regular expression from files"
msgstr ""

msgid  "Regular expression selection"
msgstr ""

msgid  "Select a regular expression from the list or from your computer"
msgstr ""

msgid  "Open regular expression"
msgstr ""

msgid  "Selected regular expression: "
msgstr ""

msgid  "The regular expression is selected randomly from files"
msgstr ""

msgid  "Grammar generated randomly"
msgstr ""

msgid  "Prefearbly select a grammar from files"
msgstr ""

msgid  "Grammar selection"
msgstr ""

msgid  "Select a grammar from the list or from your computer"
msgstr ""

msgid  "Open grammar"
msgstr ""

msgid  "Selected grammar: "
msgstr ""

msgid  "The grammar is selected randomly from files"
msgstr ""

msgid  "Save"
msgstr ""

msgid  "Save and exit"
msgstr ""

msgid  "The file was not found or you don't have enough permissions to read it. (HTTP status: {0})"
msgstr ""

msgid  "This can happen with browsers like Google Chrome or Opera when using Aude locally. This browser forbids access to files which are nedded by Aude. You might want to try Aude with another browser when using it offline. See README for more information"
msgstr ""

msgid  "Unable to get the list of needed files"
msgstr ""

msgid  "From your computer"
msgstr ""

msgid  "Load a file"
msgstr ""

msgid  "List of questions"
msgstr ""

msgid  "List of automata"
msgstr ""

msgid  "Multiple choice questions"
msgstr ""

msgid  "Complement the automaton"
msgstr ""

msgid  "Complete the automaton"
msgstr ""

msgid  "Do the product of 2 automata"
msgstr ""

msgid  "Minimize the automaton"
msgstr ""

msgid  "List all the equivalent states"
msgstr ""

msgid  "Equivalency between 2 automata"
msgstr ""

msgid  "Give the tabular form of the automaton"
msgstr ""

msgid  "Give the automaton from the table"
msgstr ""

msgid  "Give a word recognized by the automata"
msgstr ""

msgid  "Determinize the automaton"
msgstr ""

msgid  "Determinize and minimize the automaton"
msgstr ""

msgid  "Eliminate the ε-transitions"
msgstr ""

msgid  "Determinize and eliminate the ε-transitions"
msgstr ""

msgid  "Give a RE which corresponds to the automaton"
msgstr ""

msgid  "Give the automaton corresponding to the RE"
msgstr ""

msgid  "Give the automaton corresponding to the right linear grammar"
msgstr ""

msgid  "Give the right linear grammar corresponding to the automaton"
msgstr ""

msgid  "Convert the left linear grammar to the right linear grammar"
msgstr ""

msgid  "No question"
msgstr ""

msgid  "Menu questions"
msgstr ""

msgid  "Restart"
msgstr ""

msgid  "Question: "
msgstr ""

msgid  "Validate"
msgstr ""

msgid  "Display response"
msgstr ""

msgid  "Wrong answer"
msgstr ""

msgid  "Correct answer"
msgstr ""

msgid  "Initial stack symbol : "
msgstr ""

msgid  "Determinized pushdown automaton: "
msgstr ""

msgid  "State: "
msgstr ""

msgid  "Stacks"
msgstr ""

msgid  "Index"
msgstr ""

msgid  "Value"
msgstr ""

msgid  "Stack"
msgstr ""

msgid  "The algorithm you want to use needs {0} automata. Please select these automata in the order you want and click \"Continue execution\" when you are ready."
msgstr ""

msgid  "You didn’t select enough automata. Please select {0} automata."
msgstr ""

msgid  "Automaton #{0}"
msgstr ""

msgid  "Write directly the grammar: "
msgstr ""

msgid  "Or enter easily the grammar: "
msgstr ""

msgid  "Write the terminal symbols: "
msgstr ""

msgid  "Write the non terminal symbols: "
msgstr ""

msgid  "Select the start symbol"
msgstr ""

msgid  "Non terminal symbol"
msgstr ""

msgid  "Body"
msgstr ""

msgid  "Choose the entry of the algorithm: "
msgstr ""

msgid  "Choose type"
msgstr ""

msgid  "Enter a grammar"
msgstr ""

msgid  "Deterministic automaton"
msgstr "Automate déterministe"

msgid  "Non deterministic automaton"
msgstr "Automate non déterministe"

msgid "Non deterministic automaton with ε-transitions"
msgstr "Automate non déterministe avec ε-transitions"

msgid  "Settings automaton"
msgstr ""

msgid  "Pushdown automaton"
msgstr ""

msgid  "Deterministic finite state machines"
msgstr ""

msgid  "Non-deterministic finite state machines"
msgstr ""

msgid  "Non-deterministic finite state machines with ε-transitions"
msgstr ""

msgid  "Regular expressions and Kleene theorem"
msgstr ""

msgid  "Grammars and regular grammars"
msgstr ""

msgid  "Non-regular langages and iterative lemma"
msgstr ""

msgid  "Concatenate 2 automata"
msgstr ""

msgid  "Create intermediate states"
msgstr ""

msgid  "Difference"
msgstr ""

msgid  "Left linear grammar → Right linear grammar"
msgstr ""

msgid  "Linear grammar → Automaton"
msgstr ""

msgid  "Right linear grammar → Left linear grammar"
msgstr ""

msgid  "Smaller word"
msgstr ""

msgid  "Table representation → automaton"
msgstr ""

msgid  "Union of 2 automata"
msgstr ""

msgid  "Automaton → Right linear grammar"
msgstr ""

msgid  "Create automaton"
msgstr ""

msgid  "Questions list"
msgstr ""

msgid  "Start"
msgstr ""

msgid  "Stop"
msgstr ""

msgid  "Restart"
msgstr ""

msgid  "Next step"
msgstr ""

msgid  "Previous step"
msgstr ""

msgid  "Search"
msgstr ""

msgid  "Start"
msgstr ""

msgid  "Select the start state of the search: "
msgstr ""

msgid  "Animate the search"
msgstr ""

msgid  "Breadth first search"
msgstr ""

msgid  "Depth first search"
msgstr ""

msgid  "All states reachable:"
msgstr ""

msgid  "All states co-reachable :"
msgstr ""


msgid  "Enter couples of states"
msgstr ""

msgid  "Enter the answer here"
msgstr ""

msgid  "File list"
msgstr ""

msgid  "How to draw automata?"
msgstr ""

msgid "To add a new state, double-click where you want the state to be."
msgstr ""

msgid  "To add a new transition Shift+click on the start state then click on the destination state.")],
msgstr ""

msgid "To rename a state, to modify symbols of a transition, double-click on it."
msgstr ""

msgid  "To set a state as the initial state, ctrl+right click on the state."
msgstr ""

msgid "To remove a state or a transition, ctrl-click on it."
msgstr ""

msgid "Help"
msgstr ""

msgid "Other"
msgstr ""

msgid "Automata"
msgstr ""

msgid "Unit tests for Aude"
msgstr ""

msgid "Enter the regular expression you want to translate into an automaton.\n"
"Recursive syntax:\n"
" — a  is a regular expression (simple character)\n"
" — \\e and ε  are regular expressions (epsilon)\n"
" — r1 + r2  is a regular expression (union)\n"
" — r1r2, r1.r2 and r1·r2  are regular expressions (contatenation)\n"
" — r*  is a regular expression (Kleene star)\n"
"Parentheses can be used."
msgstr ""
