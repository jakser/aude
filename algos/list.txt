automaton2regex.ajs     / Automaton → Reg. Exp. / re
automaton2RightLinearGrammar.ajs / Automaton → Right linear grammar / grammar
breadthFirstSearch.ajs / Breadth first search / automaton
complementation.ajs     / Complement / automaton
completion.ajs          / Complete / automaton
complicate.ajs          / Complicate / other
concatenation.ajs / Concatenate 2 automata / automaton
createAutomaton.ajs / Create automaton / other
depthFirstSearch.ajs / Depth first search / automaton
splitWord.ajs / Create intermediate states / automaton
determinization.ajs     / Determinize / automaton
difference.ajs / Difference / automaton
equivalence.ajs         / Equiv. between 2 automata / automaton
automaton2json.ajs      / JSON representation / other
leftLinear2RightLinearGrammar.ajs / Left linear grammar → Right linear grammar / grammar
linearGrammar2Automaton.ajs / Linear grammar → Automaton / grammar
reachability.ajs        / List the reachable states / automaton
coreachability.ajs     / List the co-reachable states / automaton
distinguishability.ajs  / List differentiable states / automaton
mealy2moore.ajs         / Mealy machine → Moore machine / other
minimization.ajs        / Minimize / automaton
mirror.ajs              / Mirror / automaton
moore2mealy.ajs         / Moore machine → Mealy machine / other
mutate.ajs              / Mutate / other
product.ajs             / Product / automaton
regex2automaton.ajs     / Reg. Exp. → automaton / re
regex2minautomaton.ajs  / Reg. Exp. → minimized automaton / re
rightLinear2LeftLinearGrammar.ajs / Right linear grammar → Left linear grammar / grammar
normalization.ajs       / Simplify names of the states / automaton
smallerWord.ajs	/ Smaller word / automaton
automaton2htmltable.ajs / Table representation / automaton
htmltable2automaton.ajs	/ Table representation → automaton / automaton
emptyLanguage.ajs       / Test empty language / other
infiniteLanguage.ajs    / Test infinite language /other
union.ajs / Union of 2 automata / automaton
unitTests.ajs           / Unit tests for Aude / other
epsElimination.ajs      / ε-eliminate / automaton
