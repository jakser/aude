from normalization import normalize

# Returns a random whole number in the [minimum, maximum] interval
export function randomNumber(minimum, maximum)
    return Math.floor((Math.random()) * (maximum - minimum + 1)) + minimum
end function

# Returns a random transition from a set of transitions
export function getRandomElement(set, fallbackElement)
    return (
        if set is empty then
            fallbackElement
        else
            set.getList()[randomNumber(0, set.size - 1)]
    )
end function

# Finds a name for a new state
# in the automaton and makes sure this state name doesn't already exist
export function newStateName(A)
    let stateName := A.getStates().size
    while stateName belongs to A.getStates() do
        stateName := stateName + 1
    done
    return stateName
end function


# Changes the symbol of a random transition in A
export function changeSymbol(A)
    if A.getTransitions() is empty or A.getAlphabet().size < 2 then
        return
    end if

    # we choose a random transition of the automaton A
    let randomTransition := getRandomElement(A.getTransitions())

    # we choose a random symbol from the alphabet of A
    # different from the symbol of randomTransition
    let randomSymbol := null
    do
        randomSymbol := getRandomElement(A.getAlphabet())
    while randomSymbol = randomTransition.symbol

    A.removeTransition(randomTransition)
    A.addTransition(randomTransition.startState, randomSymbol, randomTransition.endState)
end function

# Removes a random transition from the automaton
export function removeTransition(A)
    let transitions := A.getTransitions()
    if transitions is not empty then
        A.removeTransition(getRandomElement(transitions))
    end if
end function

# Adds a random "wrong" transition in the automaton
export function addTransition(A)
    let randomInput := getRandomElement(A.getAlphabet(), "a")

    # we choose two random states of A
    let firstRandomState := getRandomElement(A.getStates())
    let secondRandomState := getRandomElement(A.getStates())

    # we add the new transition to A
    A.addTransition(firstRandomState, randomInput, secondRandomState)

    return A
end function

# Adds a random transition that starts at startState
export function addTransitionFromState(A, startState)
    let randomSymbol := getRandomElement(A.getAlphabet(), "a")
    let randomState := getRandomElement(A.getStates())
    A.addTransition(startState, randomSymbol, randomState)
    return A
end function

# Adds an epsilon transition that starts at startState
export function addEpsilonTransitionFromState(A, startState)
    let randomState := getRandomElement(A.getStates())
    A.addTransition(startState, epsilon.toString(), randomState)
    return A
end function

# Adds a random transition that arrives at endState
export function addTransitionToState(A, endState)
    let randomSymbol := getRandomElement(A.getAlphabet())
    let randomState := getRandomElement(A.getStates())
    A.addTransition(randomState, randomSymbol, endState)
    return A
end function

# Adds an epsilon transition transition that arrives at endState
export function addEpsilonTransitionToState(A, endState)
    let randomState := getRandomElement(A.getStates())
    A.addTransition(randomState, epsilon.toString(), endState)
    return A
end function


# Adds a new state to an automaton
export function addState(A, stateName)
    if stateName = undefined then
        stateName := newStateName(A)
    end if
    A.addState(stateName)
    return stateName
end function

# Adds a new state and new transitions towards and from this state
export function addStateAndTransitions(A)
    let newState := addState(A)

    let random := randomNumber(1, 3)
    let i := 0
    while i < random do
        addTransitionToState(A, newState)
        i++
    done

    random := randomNumber(1, 3)
    i:=0
    while i < random do
        addTransitionFromState(A, newState)
        i++
    done
end function

# REMARK : to add a new state and transitions to this state
# use addState(A, stateName) and addTransitionToState(A, stateName)

# Inverses a random transition of the automaton
export function inverseTransition(A)
    # if endState = startState then this function will not change the automaton
    # so we'll make sure to check the transition before inversing it

    # we put in the T set all the transitions that have a distinct startState and endState
    let T := new Set()
    foreach transition in A.getTransitions() do
        if transition.startState != transition.endState then
            T.add(transition)
        end if
    end foreach

    if T is not empty then
        let randomTransition := getRandomElement(T)
        A.removeTransition(randomTransition)
        A.addTransition(randomTransition.endState, randomTransition.symbol, randomTransition.startState)
    end if
end function

# Takes an automaton and returns another automaton identical to A missing a final state
export function removeFinalState(A)
    if A.getFinalStates() is not empty then
        A.toggleFinalState(getRandomElement(A.getFinalStates()))
    end if
end function

# If the automaton contains epsilon transitions
# then this function removes one of the epsilon transitions from the automaton
export function removeEpsilonTransition(A)
    # we count the number of epsilon transitions present in A
    let counter := 0
    foreach transition in A.getTransitions() do
        if transition.symbol = 'ε' then
            counter++
        end if
    end foreach

    if counter = 0 then
        return # A has no epsilon transitions
    end if

    # FIXME think about doing the other method where we store the epsilon transitions at
    # the beginning and then we won't have to go over all of the transitions twice

    let random := randomNumber(1, counter)
    let i := 1
    foreach transition in A.getTransitions() do
        if transition.symbol = epsilon then
            if i = random then
                A.removeTransition(transition)
                return A
            else
                i++
            end if
        end if
    end foreach
end function

# Removes a state from the automaton that isn't the initial state
export function removeState(A)
    let statesWithoutInitial := A.getStates() \ {A.getInitialState()}

    if statesWithoutInitial is not empty then
        A.removeState(getRandomElement(statesWithoutInitial))
    end if
end function


# Transforms a random state into a final state
export function addFinal (A)
    let nonFinalStates := A.getStates() \ A.getFinalStates()
    if nonFinalStates is not empty then
        A.toggleFinalState(getRandomElement(nonFinalStates))
    end if
end function

run(
    function(A)
        let input := window.prompt(
            AudeGUI.l10n(
                "Transform the current automaton by choosing one or more of the following options:\n1 - Change a symbol\n2 - Remove a transition\n3 - Add a transition\n4 - Add a state and some transitions\n5 - Inverse a transition\n6 - Rename states\n7 - Make a state non accepting\n8 - Remove an epsilon transition\n9 - Remove a state\n10 - Make a state accepting"
            ),
            "2 1 10"
        )

        # extracting from input the set of numbers separated by commas/spaces/anything_else_that's_not_a_number
        let I := []
        let n := 0
        foreach c in input do
            if c <= '9' and c >= '0' then
                n := n * 10 + parseInt(c)
            else
                I.push(n)
                n := 0
            end if
        done
        if n !=0 then
            I.push(n)
        end if

        # for each case we execute the associated function
        foreach c in I do
           if c = 1 then
               changeSymbol(A)
           else if c = 2 then
               removeTransition(A)
           else if c = 3 then
               addTransition(A)
           else if c = 4 then
               addStateAndTransitions(A)
           else if c = 5 then
               inverseTransition(A)
           else if c = 6 then
               normalize(A)
           else if c = 7 then
               removeFinalState(A)
           else if c = 8 then
               removeEpsilonTransition(A)
           else if c = 9 then
               removeState(A)
           else if c = 10 then
               addFinal(A)
           end if
        end foreach
        return A
    end function,
    get_automaton(currentAutomaton)
)
