# Return an array containing the name of states
# Take an automaton and the state to begin
# Can also take the trace of the algorithm

let l10n := AudeGUI.l10n

export function depthFirstSearch (A,start,trace)

    if start!= "" and isNaN(start)=false then # Convert the string number to an int
        start:=parseInt(start)
    fi

    let source := start
    if start=undefined or start=null or start="" then
        source :=  A.getInitialState() # The first state
    fi

    let stack := [] # The stack to store the states to visit
    stack.push(source)

    let visited := {} # Set of visited states
    visited.add(source)

    let listState := [] # The list of states in order

    if trace = undefined then # Doesn't need to fill the trace
        while stack is not empty do
            let s := stack.pop() # Take the first element of the file
            listState.push(s)
            foreach t in A.getSuccessors(s) do
                if t does not belong to visited then
                    stack.push(t)
                    visited.add(t)
                fi
            done
        done
    else # Need to fill the trace
        while stack is not empty do
            let s := stack.pop() # Take the first element of the file
            listState.push(s)
            let step := {} # step is composed of the state explored and the new states to visite
            step.state := String(s)
            step.toVisite := []
            foreach t in A.getSuccessors(s) do
                if t does not belong to visited then
                    stack.push(t)
                    visited.add(t)
                    step.toVisite.push(String(t))
                fi
            done
            trace.push(step)
        done
    end if

    return listState
end function



# Animate the search
# Take trace of the algorithm, the automaton and time between two steps, the next step to execute and stop
function animateSearch(trace,A,timeStep,iStep,stop)
    if stop.value = 0 then
        if iStep.value = trace.length then # End of the search
            removeColorAutomaton(A) # Delete all the colors added during the animation
            iStep.value := 0
            buttonStart(stop)
        else
            color(trace[iStep.value].state,trace[iStep.value].toVisite)
            iStep.value ++
            setTimeout(animateSearch,timeStep,trace,A,timeStep,iStep,stop)
        end if
    end if
end function

# color in black the state visited and in grey the states to visit
function color(stateVisited,stateToVisite)
    AudeGUI.mainDesigner.stateSetBackgroundColor(AudeGUI.mainDesigner.currentIndex,stateVisited,"rgba(0,0,0,0.5)")
    foreach state in stateToVisite do
        AudeGUI.mainDesigner.stateSetBackgroundColor(AudeGUI.mainDesigner.currentIndex,state,"rgba(150,150,150,0.5)")
      done
end function

# color in grey the state visited and in white the states to visit
function removeColor(stateVisited,stateToVisite)
    AudeGUI.mainDesigner.stateSetBackgroundColor(AudeGUI.mainDesigner.currentIndex,stateVisited,"rgba(150,150,150,0.5)")
    foreach state in stateToVisite do
        AudeGUI.mainDesigner.stateSetBackgroundColor(AudeGUI.mainDesigner.currentIndex,state,"rgba(255,255,255)")
      done
end function

# Next step of the animation
# Take trace of the algorithm and the automaton
export function animateSearchNextStep(trace,A,iStep)
    if iStep.value = trace.length then # End of the animation
        removeColorAutomaton(A)# Delete all the colors added during the animation
        iStep.value:=0
    else
        color(trace[iStep.value].state,trace[iStep.value].toVisite)
        iStep.value++
    fi
end function

# Previous step of the animation
# Take trace of the algorithm and the automaton
function animateSearchPreviousStep(trace,A,iStep)
    if iStep.value = 0 then
        foreach t in trace do
            AudeGUI.mainDesigner.stateSetBackgroundColor(AudeGUI.mainDesigner.currentIndex,t.state,"rgba(0,0,0,0.5)")
        done
        iStep.value := trace.length
    else if iStep.value = 1 then
        removeColorAutomaton(A)
        iStep.value --
    else
        iStep.value --
        removeColor(trace[iStep.value].state,trace[iStep.value].toVisite)
    fi
end function

# Remove the colors of the automaton
function removeColorAutomaton(A)
    foreach state in A.getStates() do
        AudeGUI.mainDesigner.stateSetBackgroundColor(AudeGUI.mainDesigner.currentIndex,String(state),"rgb(255,255,255)")
    done
end function

# The menu to select start state, and if it's animated
# fSearch is the function of the search (depthFirstSearch,breadthFirstSearch)
export function displayMenu(A,fSearch)
    let div := libD.jso2dom([
        ["div#div-menu-search.libD-ws-colors-auto",[
            ["span", l10n("Select the start state of the search: ")],
            ["select#input-start-state",[
            ]],
            ["div#div-animation-search",[
                ["button#button-execute-search",("Execute")], # Execute the search
                ["br"],
                ["span", l10n("Animate the search")],
                ["input#is-animated",{"type":"checkbox"}],
                ["div#div-run-animation",{"style":"display:none"},[
                    ["span", l10n("Delay between steps (ms): ")],
                    ["input#input-execute-delay",{"type":"text", "value":"500"}],["br"],
                    ["button#button-search-start.symbol-animation-search",{"title":l10n("Start")},("►")],
                    ["button#button-search-previous.symbol-animation-search",{"title" :l10n("Previous Step")},("↤")],
                    ["button#button-search-next.symbol-animation-search",{"title" :l10n("Next Step")},("↦")],
                    ["button#button-search-restart",l10n("Restart")],
                ]],
            ]],
        ]],
    ])
   let  settingsWin := libD.newWin({
        "minimizable": false,
        "title": l10n("Search"),
        "show": true,
        "content" : div
    })
    let selStartState := document.getElementById("input-start-state")

    foreach s in A.getStates() do # Add the state to the selection of the start state
        var option := document.createElement("option")
        option.value := s
        option.textContent := s
        selStartState.appendChild(option)
    done
    selStartState.value := A.getInitialState() # By default the start state is the initial state

    let anima := document.getElementById("is-animated")
    anima.onchange := function () # Show and hide the animation section
        if anima.checked then
            document.getElementById("div-run-animation").style.display := "inherit"
            settingsWin.resize()
        else
            document.getElementById("div-run-animation").style.display := "none"
            settingsWin.resize()
        fi
    end function

    # Execute the search without animation
    document.getElementById("button-execute-search").onclick := function ()
        var list := fSearch(A,selStartState.value)
        AudeGUI.Results.set(list)
    end function


    var iStep := {} # The next step to animate
    var stop := {} # If stop=1 then stop the animation
    iStep.value := 0
    stop.value := 1
    var trace := [] # Trace of the algorithm
    # Launch/Stop the animation of the search
    document.getElementById("button-search-start").onclick := function ()
        if stop.value = 0 then
            buttonStart(stop)
        else
            buttonStop(stop)
            if trace.length = 0 then
                fSearch(A,selStartState.value,trace)
            end if
            animateSearch(trace,A,parseInt(document.getElementById("input-execute-delay").value),iStep,stop)
        end if
    end function

    # Next step of the execution
    document.getElementById("button-search-next").onclick := function ()
        if trace.length = 0 then
            fSearch(A,selStartState.value,trace)
        end if
        animateSearchNextStep(trace,A,iStep)
    end function

    # Previous step of the execution
    document.getElementById("button-search-previous").onclick := function ()
        if trace.length = 0 then
            fSearch(A,selStartState.value,trace)
        end if
        animateSearchPreviousStep(trace,A,iStep)
    end function

    # If we change the start state, it stops the current execution
    document.getElementById("input-start-state").onchange := function ()
        if document.getElementById("is-animated").checked then
            buttonStart(stop)
            trace := []
            iStep.value := 0
            removeColorAutomaton(A)
        end if

    end function

    # Replace at the beginning of the search
    document.getElementById("button-search-restart").onclick := function ()
        if document.getElementById("is-animated").checked then
            buttonStart(stop)
            trace := []
            iStep.value := 0
            fSearch(A,selStartState.value,trace)
            removeColorAutomaton(A)
        end if
    end function
end function

# Change the button to the start mode
function buttonStart(stop)
    stop.value := 1 # Stop the animation
    document.getElementById("button-search-start").textContent := "►"
    document.getElementById("button-search-start").title := l10n("Start")
end function
# Change the button to the stop mode
function buttonStop(stop)
    stop.value := 0
    document.getElementById("button-search-start").textContent := "▪"
    document.getElementById("button-search-start").title := l10n("Stop")
end function

run(
    function()
    let A := get_automaton(currentAutomaton)
    displayMenu(A,depthFirstSearch)
    end function
)
