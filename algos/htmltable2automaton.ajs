# Take an html Table and convert it into an automaton
export function HTMLTable2automaton (table,audeInter)
    let A := new Automaton()

    # The inputs state
    let sta := table.getElementsByClassName("div-container-input-states")[0].childNodes
    # The inputs symbol
    let sym := table.getElementsByClassName("div-container-input-symbols")[0].childNodes

    let tabState := []
    let tabSymbol := []
    # Initialise states and final states
    foreach state in sta do
        tabState.push(String(/[^\*>-]+/.exec(state.value)))
        if /\->[^\*>-]+\*/.test(state.value) then # Initial and final state
            A.addFinalState(String(/[^\*>-]+/.exec(state.value)))
            A.setInitialState(String(/[^*>-]+/.exec(state.value)))
        else if /\*/.test(state.value) then # Final state
            A.addFinalState(String(/[^\*>-]+/.exec(state.value)))
        else if /->/.test(state.value) then # Initial state
            A.setInitialState(String(/[^*>-]+/.exec(state.value)))
        else # Normal State
            A.addState(state.value)
        end if
    end for

    # Initialise alphabet
    for each c in sym do
        if String(c.value)="epsilon" or String(c.value)="\\e" then # To put epsilon
            A.setAlphabet([epsilon])
            tabSymbol.push(epsilon)
        else
            A.setAlphabet(c.value)
            tabSymbol.push(c.value)
        end if
    end for

    # Initialise transition
    let trans := document.getElementsByClassName("div-container-input-transition")[0].childNodes
    let numCol := 0
    let numLine := 0
    let listSates := undefined
    let aLine := null
    foreach line in trans do
        aLine := line.childNodes # The list of input
        foreach cell in aLine do # For each input
            if /,/.test(cell.value) then
                listSates := cell.value.split(',')
            else if cell.value.length=0 then
                listSates := null
            else
                listSates := [cell.value]
            end if

            if listSates != undefined  then # If there is a value in the cell
                foreach s in listSates do
                    if s belongs to tabState then
                        A.addTransition(tabState[tabState.length-1-numCol],tabSymbol[numLine],s)
                    else
                        throw new Error("Try to create a transition toward a state that doesn't exist!")
                    end if
                end for
            end if
            listSates := undefined
            numCol ++
        end for
        numLine ++
        numCol :=0
    end for

    # Print the response
    if audeInter != undefined then
        AudeGUI.Results.set(A)
    else
        return A
    end if
end function

# Create a table into divAffi
let  settingsWin := null
export function createTable (audeInter,div)
    if document.getElementById("div-container-table")=null or document.getElementById("div-container-table")=undefined then
        let divTable := (libD.jso2dom([
                ["div#div-container-table",[
                    ["button",{"class":"button-table-automaton"},("Add state")],
                    ["button",{"class":"button-table-automaton"},("Add symbol")],
                    ["button",{"class":"button-table-automaton"},("Remove state")],
                    ["button",{"class":"button-table-automaton"},("Remove symbol")],
                    ["button",{"class":"button-table-automaton"},("Create Automaton")],
                    ["br"],
                    ["div#div-container-input-automaton",[
                        ["div",{"class":"div-container-input-states"}, [
                            ["input",{"type":"text","class":"cell-input-automaton"}],
                            ["input",{"type":"text","class":"cell-input-automaton"}],
                        ]],
                        ["div",{"class":"div-container-input-line2-automaton"},[
                            ["div",{"class":"div-container-input-symbols"},[
                                ["input",{"type":"text","class":"cell-input-automaton"}],
                                ["input",{"type":"text","class":"cell-input-automaton"}],
                            ]],
                            ["div",{"class":"div-container-input-transition"},[
                                ["div",{"class":"div-container-input-line-automaton"},[
                                    ["input",{"type":"text","class":"cell-input-automaton"}],
                                    ["input",{"type":"text","class":"cell-input-automaton"}],
                                ]],
                                ["div",{"class":"div-container-input-line-automaton"},[
                                    ["input",{"type":"text","class":"cell-input-automaton"}],
                                    ["input",{"type":"text","class":"cell-input-automaton"}],
                                ]],
                            ]],
                        ]],
                    ]],
                ]],
            ]))
        if audeInter != undefined and audeInter != "" then
            settingsWin := libD.newWin({
                "minimizable": false,
                "title": ("Create automaton from table"),
                "show": true,
                "content" : divTable
            })
        else
            div.appendChild(divTable)
        end if

        document.getElementsByClassName("cell-input-automaton")[1].value:=("->1")
        document.getElementsByClassName("cell-input-automaton")[0].value:=("2")
        document.getElementsByClassName("cell-input-automaton")[2].value:=("a")
        document.getElementsByClassName("cell-input-automaton")[3].value:=("b")
        let buttons := document.getElementsByClassName("button-table-automaton")
        buttons[0].onclick:=addState
        buttons[1].onclick:=addSymbol
        buttons[2].onclick:=removeState
        buttons[3].onclick:=removeSymbol
        buttons[4].onclick:= HTMLTable2automaton.bind(null,document.getElementById("div-container-input-automaton"),audeInter)
    else
        throw new Error("Already launched the program")
    end if
end function

# Add a new column
function addState ()
    settingsWin.resize() # Resize the window
    let tabLine := document.getElementsByClassName("div-container-input-line-automaton") # Get all the line
    let tabStates := document.getElementsByClassName("div-container-input-states")
    foreach line in tabLine do
        line.appendChild(libD.jso2dom([
            ["input",{"type":"text","class":"cell-input-automaton"}]
        ]));
    end for
    tabStates[0].insertBefore((libD.jso2dom([
        ["input",{"type":"text","class":"cell-input-automaton"}]
    ])),tabStates[0].childNodes[0])
end function

# Remove the last column
function removeState ()
    settingsWin.resize() # Resize the window
    let tabStates := document.getElementsByClassName("div-container-input-states")
    if tabStates[0].childNodes.length>1 then
        let tabLine := document.getElementsByClassName("div-container-input-line-automaton")
        foreach line in tabLine do
            line.removeChild(line.childNodes[line.childNodes.length-1])
        end for
        tabStates[0].removeChild(tabStates[0].childNodes[0])
    end if
end function

# Add a new line
function addSymbol ()
    settingsWin.resize() # Resize the window
    let tabLine := document.getElementsByClassName("div-container-input-line-automaton")
    let nbColumn := tabLine[0].childNodes.length # Number of column to add

    # Add the line
    let tab := document.getElementsByClassName("div-container-input-transition")
    tab[0].appendChild(libD.jso2dom([
        ["div",{"class":"div-container-input-line-automaton"}]
    ]));

    # Add a new cell in the symbols
    let tabSym := document.getElementsByClassName("div-container-input-symbols")
    tabSym[0].appendChild(libD.jso2dom([
        ["input",{"type":"text","class":"cell-input-automaton"}]
    ]));

    # Add the column in the new line
    let line := document.getElementsByClassName("div-container-input-line-automaton")
    for i from 1 to nbColumn do
        line[line.length-1].appendChild(libD.jso2dom([
            ["input",{"type":"text","class":"cell-input-automaton"}]
        ]));
    end for
end function

# Remove the last line
function removeSymbol ()
    settingsWin.resize() # Resize the window
    let sym := document.getElementsByClassName("div-container-input-symbols")
    if sym[0].childNodes.length >1 then
        let trans := document.getElementsByClassName("div-container-input-transition") # Get the container
        trans[0].removeChild(trans[0].childNodes[trans[0].childNodes.length-1])
        sym[0].removeChild(sym[0].childNodes[sym[0].childNodes.length-1])
    end if
end function

run(
    function ()
        createTable(1)
    end function
)
